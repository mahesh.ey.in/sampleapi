package com.api.sample.repo;

import com.api.sample.model.Sample;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleRepository extends JpaRepository<Sample,Integer> {
}
