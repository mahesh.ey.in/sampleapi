package com.api.sample.controller;

import com.api.sample.exception.ResourceNotFoundException;
import com.api.sample.model.Sample;
import com.api.sample.repo.SampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class SampleController {
    @Autowired
    private SampleRepository repository;
    //getall
    @GetMapping("/all")
    public List<Sample> getAll(){
        return repository.findAll();
    }
    //getById
    @GetMapping("/id/{id}")
    public Optional<Sample> getById(@PathVariable Integer id){
        return repository.findById(id);
    }
    //DeleteById
    @DeleteMapping("/delete/id/{id}")
    public String delete(@PathVariable Integer id){
        repository.deleteById(id);
        return "DETAILS OF ID"+id+"DELETED SUCCESSFULLY";
    }
    //UpdateById-by posting present id data through body.
    @PutMapping("/update/{id}")
    public ResponseEntity<Sample> updateProd(@RequestBody Sample p, @PathVariable Integer id)
            throws ResourceNotFoundException {
        Sample sample=repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Sample not found for this id :: " + id));
        sample.setName(p.getName());
        sample.setCity(p.getCity());
        final Sample updProd=repository.save(sample);
        return ResponseEntity.ok(updProd);

    }
}
